terraform {
  backend "s3" {
    key="terraform/backend_ansible_vms"
    region="eu-west-1"
    bucket="anthony.grohar.bucket"
  }
}

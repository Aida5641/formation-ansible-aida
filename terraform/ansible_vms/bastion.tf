
################################################################################
# Compute Resources
################################################################################
resource "aws_instance" "bastion" {
  ami                         = "ami-08edbb0e85d6a0a07" # UBUNTU # "ami-04dd4500af104442f" # AWS-AMI # data.aws_ami.ubuntu.id
  subnet_id                   = aws_subnet.devops_admin_subnet.id
  instance_type               = "t2.micro"
  associate_public_ip_address = false
  security_groups             = [aws_security_group.bastion_sg.id]
  key_name                    = local.key_name
  
  tags = {
    Name = "bastion_vm_${var.participant}"
    participant = "${var.participant}"
    type = "bastion"
  }
}

################################################################################
# Network Resources
################################################################################
data "aws_subnet" "devops_work_subnet" {
  tags = {
    Name = "devops_work_subnet"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.devops_admin_subnet.id
  route_table_id = aws_route_table.devops_nat_rt.id
}

resource "aws_subnet" "devops_admin_subnet" {
  vpc_id     = data.aws_vpc.devops_vpc.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "devops_admin_subnet"
  }
}

resource "aws_route_table" "devops_nat_rt" {
  vpc_id = data.aws_vpc.devops_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat_gateway_appli.id
  }

  tags = {
    Name = "devops_nat_rt"
  }
}
resource "aws_eip" "elastic_ip" {
  vpc = true
}

# For private VMs internet access
resource "aws_nat_gateway" "nat_gateway_appli" {
  allocation_id = aws_eip.elastic_ip.id
  subnet_id = aws_subnet.devops_front_subnet.id

  tags = {
    Name = "nat-gateway"
  }
}

resource "aws_security_group" "bastion_sg" {
  name   = "bastion_sg"
  vpc_id = data.aws_vpc.devops_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [data.aws_subnet.devops_work_subnet.cidr_block] 
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

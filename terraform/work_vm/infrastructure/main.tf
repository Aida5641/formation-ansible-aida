# doc https://github.com/antonputra/tutorials/tree/main/lessons/014

provider "aws" {
	profile    = "default"
	region     = "eu-west-1"
}

terraform {
  backend "s3" {}
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

################################################################################
# Compute Resources
################################################################################

resource "aws_internet_gateway" "devops_gw" {
  vpc_id = aws_vpc.devops_vpc.id

  tags = {
    Name = "main"
  }
}

resource "aws_route_table" "devops_rt" {
  vpc_id = aws_vpc.devops_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.devops_gw.id
  }

  tags = {
    Name = "devops_rt"
  }
}

resource "aws_vpc" "devops_vpc" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "devops_vpc"
  }
}

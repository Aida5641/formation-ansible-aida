# Premier TP Ansible

## Informations importantes
Ce dossier comprend le premier TP de cette formation Ansible (sous dossier exercice) et la correction associée<br>
Le but de ce TP est de prendre en main les notions de base d'Ansible : Playbook, Plays, Tasks et Module, le tout pour une éxécution en localhost

## Comment dérouler ce TP ?
Il suffit de compléter les 3 playbooks du dossier Exercice <br>
- Le premier est sous la forme d'un code à trous pour présenter la syntaxe d'un play Ansible, l'idée étant d'utiliser Ansible pour faire de la gestion de fichiers/dossiers<br>
- Le deuxième playbook a pour but d'installer et de démarrer un apache. Penser à reprendre la syntaxe présenter du premier playbook plutôt que de repartir from scratch<br>
- Pour le troisième playbook, il ne sert qu'à appeler les deux premiers, servant à démontrer les principes d'inclusions/importation et d'idempotence (on se retrouve dans le même état après l'éxécution du playbook 3 qu'après celle des deux premiers)<br>
<br>
Pour éxécuter les playbooks il faut éxécuter la commande suivante (évidemment remplacer par playbook2.yml et playbook3.yml en fonction du playbook que vous souhaitez appeler)

```
ansible-playbook playbook1.yml
```

La correction s'éxécute avec la même commande sans prérequis particulier

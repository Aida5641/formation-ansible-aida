# Deuxième TP Ansible

## Informations importantes
Ce dossier comprend le deuxième TP de cette formation Ansible (sous dossier exercice) et la correction associée<br>
Le but de ce TP est de reprendre les notions de base pour installer un apache, créer un fichier html et l'exposer à l'aide de l'apache

## Comment dérouler ce TP ?
Il suffit de compléter le playbook du dossier Exercice <br>
Dans le playbook, il est demander de créer un fichier html et de le modifier, fichier qui servira de page d'exposition de votre apache. Si vous le souhaitez, vous pouvez créer hors Ansible une page html plus poussée ou en récupérer une sur internet, à votre convenance <br>
Pour éxécuter le playbook il faut éxécuter la commande suivante
```
ansible-playbook playbook1.yml
```
La correction s'éxécute avec la même commande sans prérequis particulier

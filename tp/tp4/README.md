# Quatrième TP  Ansible

## Informations importantes
Ce dossier comprend le quatrième TP de cette formation Ansible (sous dossier exercice) et la correction associée<br>
Le but de ce TP est de gérer la configuration d'un reverse proxy et d'un apache avec Ansible

## Comment dérouler ce TP ?
Il suffit de compléter les fichiers suivants de la partie exercice (si possible dans cet ordre) : <br>
- le fichier inventory/inventory.yml qui permet de décrire la vision qu'Ansible va avoir de votre infrastructure, ici le bastion et le reverse<br>
- Le fichier ssh.cfg qui va vous permettre de gérer vos options de connexion SSH et de forcer un rebond par votre Bastion<br>
- Les fichiers roles/apache/tasks/main.yml et roles/apache/handlers/main.yml qui vont vous permettre de décrire respectivement les tâches et handlers de votre rôle apache, le but de ce rôle étant d'installer un apache, de changer le fichier html exposé par défaut, de changer le port d'exposition et de redémarer le service<br>
- Les fichiers roles/nginx/tasks/main.yml et roles/nginx/handlers/main.yml qui vont permettre d'installer un nginx, de changer sa conf pour rediriger les flux entrants vers le serveur appli et de redémarrer le service <br>
- Le fichier roles/nginx/files/nginx.conf où il faut renseigner l'adresse du serveur applicatif pour rediriger vers ce même serveur <br>
- Le fichier playbook1.yml qui va permettre d'appeler le rôle apache sur le serveur appli et le rôle nginx sur le reverse<br>

Une fois ces fichiers complétés, il faut éxécuter votre code ansible à l'aide de la commande suivante
```
ansible-playbook playbook1.yml -i inventory/
```
## Comment éxécuter la correction ?
Contrairement au TP 1 qui s'éxécutait en localhost, la correction ne peut pas être directement éxécutée dans ce TP
Il faudra d'abord compléter les fichiers inventory/inventory.yml avec les adresses du bastion et du reverse et le fichier ssh.cfg avec l'adresse du Bastion dans les deux blocs, puis le fichier roles/nginx/files/nginx.conf en renseignant l'adresse du serveur appli
Une fois ceci fait, le code peut-être éxécuté avec la commande sus-nommée

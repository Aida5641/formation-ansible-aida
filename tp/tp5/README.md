# Cinquième TP Ansible

## Informations importantes
Ce dossier comprend le cinquième TP de cette formation Ansible (sous dossier exercice) et la correction associée<br>
Le but de ce TP est de reprendre le code précédent mais en fonctionnant avec un plugin d'inventaire dynamique

## Comment dérouler ce TP ?
Avant toute chose, il faut en premier lieu installer boto3, qui est le SDK d'AWS. Ce paquet est nécessaire pour faire fonctionner l'inventaire dynamique d'Ansible sur AWS
Il faut pour cela lancer la commande 'pip install boto3' <br>
Une fois boto3 installé, il suffit de compléter les fichiers suivants de la partie exercice (si possible dans cet ordre) : <br>
- CREER un fichier 01-aws_ec2.yml (il doit avoir ce nom) dans le dossier d'inventaire. Il faudra y renseigner "plugin: aws_ec2" ainsi que la région (eu-west-1) et les valeurs des attributs aws_access_key et aws_secret_key <br>
- 02-platform.yml qui va permettre de rajouter une surcouche par-dessus notre fichier d'inventaire pour créer des groupes ansible supplémentaires<br>
- le fichier playbook1.yml où il faudra renseigner les noms des groupes fraîchement créés

Une fois ces fichiers complétés, il faut éxécuter votre code ansible à l'aide de la commande suivante
```
ansible-playbook playbook1.yml -i inventory/
```
## Comment éxécuter la correction ?
Pour éxécuter la correction, il faudra simplement créer le fichier 01-aws_ec2.yml de la même manière que pour la partie exercice et d'éxécuter la commande ci-dessus
